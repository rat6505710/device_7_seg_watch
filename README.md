# Описание
Репозиторий содержит код ПО устройства на языке The Rat (реализация C-- для AVR) для микроконтроллера ATtiny13a.

# Назначение устройство
Устройство сочетает в себе следующие функции:
1. Часы (двадцати четырех часовой формат, без даты, без летнего времени)
1. Будильник (до десяти меток времени)
1. Таймер (от 1 минуты до 2 часов, шаг 1 минута)
1. Генератор случайных чисел (количество разрядов от 1 до 4)
1. Секундомер (до 99 минут, 99 секунд)
1. Счетчик нажатий (от 0 до 9990, с возможностью обртного счета)

# Конфигурация аппаратной части
1. Фьюз-биты микроконтроллера стандартные, но убран делитель на 8 (тактовая частота ядра 9.6 МГц)
1. Все клавиши подключены к PB0 (вход ADC) через R2R схему
1. Выводы программного SPI MOSI (PB3) и CLK (PB2) подключены к сборке из двух сдвиговых регистров HC595, соединенных последовательно.
1. Вывод B4 является стробом для выдачи данных из внутреннего буфера сдвиговых регистров на выводы.
1. В качестве семисегментного индикатора с четырьмя разрядами и общим анодом может выступать A56-12SRWA или аналогичный. Контакты индикатора DIG1-DIG4 подключены к контактам Q0-Q3 первого от микроконтроллера сдвигового регистра соответственно. Контакты A-G индикатора подключены к контактам Q0-Q6 второго от микроконтроллера сдвигового регистра через резисторы 220 Ом соответственно. 
1. Активный бизер подключен к выводу Q4 первого от микроконтроллера сдвигового регистра

# Особенности проекта
Используется стартовый вектор прерываний, таймер, прерывание от таймера. Таймер настроен на частоту обновления сегмента. В прерывании от таймера только устанавливается флаг срабатывания прерывания. Основная работа происходит в main.

# Состав проекта
1. Директория build - содержит файл (main.hex) с прошивкой микроконтроллера. После сборки скриптом build.sh доступен еще и main.hex.map. В репозитории main.hex.map не хранится.
1. Файл build.sh - скрипт сборки проекта. Предполагает наличие в переменной PATH директории с компилятором
1. Директория driver_attiny13a - субмодуль с полезными inline методами
1. Файл src/main.art - содержит исходный код проекта